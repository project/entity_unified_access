<?php

namespace Drupal\entity_unified_access\QueryAccess;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SqlQueryAlter implements ContainerInjectionInterface {

  /**
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(ContainerInterface $container, \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager) {
    $this->container = $container;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container,
      $container->get('entity_type.manager')
    );
  }

  /**
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function alter(SelectInterface $query) {
    foreach ($this->entityTypeManager->getDefinitions() as $entityTypeId => $entityType) {
      if ($query->hasTag("{$entityTypeId}_access")) {
        $this->createInstance($query, $entityType)->alter();
      }
    }
  }

  /**
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *
   * @return \Drupal\entity_unified_access\QueryAccess\SqlQueryAlterEntityType
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createInstance(SelectInterface $query, EntityTypeInterface $entityType) {
    $storage = $this->entityTypeManager->getStorage($entityType->id());
    if ($storage instanceof SqlContentEntityStorage) {
      $tableMapping = $storage->getTableMapping();
    }
    if (!$tableMapping instanceof DefaultTableMapping) {
      throw new \LogicException("Entity unified access needs SQL default storage. Found something else on {$entityType->id()}.");
    }
    return new SqlQueryAlterEntityType(
      $this->container->get('entity.unified_access.dispatcher'),
      $this->container->get('entity.unified_access.cacheability_utility'),
      $this->container->get('database'),
      $query,
      $entityType,
      $tableMapping
    );
  }

}
