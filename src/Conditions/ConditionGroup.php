<?php

namespace Drupal\entity_unified_access\Conditions;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyTrait;

/**
 * Represents a group of query access conditions.
 *
 * Used by query access handlers for filtering lists of entities based on
 * granted permissions.
 *
 * Examples:
 * @code
 *   // Filter by node type and uid.
 *   $condition_group = new AndConditionGroup('type&uid');
 *   $condition_group->addCondition('type', ['article', 'page']);
 *   $condition_group->addCondition('uid', '1');
 *
 *   // Filter by node type or status.
 *   $condition_group = new OrConditionGroup('type|status');
 *   $condition_group->addCondition('type', ['article', 'page']);
 *   $condition_group->addCondition('status', '1', '<>');
 *
 *   // Nested condition groups: node type AND (uid OR status).
 *   $condition_group = new AndConditionGroup('article|page|...');
 *   $condition_group->addCondition('type', ['article', 'page']);
 *   $condition_group->addCondition((new OrConditionGroup('uid|status'))
 *     ->addCondition('uid', 1)
 *     ->addCondition('status', '1')
 *   );
 * @endcode
 */
abstract class ConditionGroup extends ConditionBase implements ConditionInterface, \Countable {

  use CacheableDependencyTrait;
  use CreateByNameTrait;

  /**
   * The conditions.
   *
   * @var \Drupal\entity_unified_access\Conditions\ConditionInterface[]
   */
  protected $conditions = [];

  /**
   * Gets the conjunction.
   *
   * @return string
   *   The conjunction. Possible values: AND, OR.
   */
  abstract public function getConjunction();

  /**
   * Gets all conditions and nested condition groups.
   *
   * @param bool $simplify
   *   Whether irrelevant conditions (FALSE in OR, TRUE in AND) are filtered.
   *   Changing filtered conditions reference is useless though.
   *
   * @return \Drupal\entity_unified_access\Conditions\ConditionInterface[]
   *   The conditions, where each one is either a Condition or a nested
   *   ConditionGroup. Returned by reference, to allow callers to replace
   *   or remove conditions.
   */
  public function &getConditions($simplify = FALSE) {
    if ($simplify) {
      $filteredConditions = array_filter($this->conditions, function (ConditionInterface $condition) {
        return $condition->getConstantValue() !== $this->getEmptyValue();
      });
      return $filteredConditions;
    }
    else {
      return $this->conditions;
    }
  }

  /**
   * Adds a condition.
   *
   * @param \Drupal\entity_unified_access\Conditions\ConditionInterface $condition
   *   Either a condition group (for nested AND/OR conditions), or a
   *   condition.
   *
   * @return $this
   */
  public function add($condition) {
    $this->conditions[] = $condition;
    return $this;
  }

  /**
   * Clones the contained conditions when the condition group is cloned.
   */
  public function __clone() {
    foreach ($this->conditions as $i => $condition) {
      $this->conditions[$i] = clone $condition;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    $lines = [];
    foreach ($this->conditions as $condition) {
      $lines[] = str_replace("\n", "\n  ", (string) $condition);
    }
    return $lines ? "/*{$this->name}*/ (\n  " . implode("\n    {$this->getConjunction()}\n  ", $lines) . "\n)" : '';
  }

  /**
   * {@inheritdoc}
   */
  public function count(): int {
    return count($this->conditions);
  }

  /**
   * Gets the constant value for an empty condition group.
   *
   * @return bool
   *   The value.
   */
  abstract public function getEmptyValue();

  /**
   * @inheritDoc
   */
  public function getConstantValue() {
    foreach ($this->conditions as $condition) {
      $value = $condition->getConstantValue();
      if (isset($value) && $value !== $this->getEmptyValue()) {
        return $value;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function isDependentOnThisEntity() {
    foreach ($this->conditions as $condition) {
      if ($condition->isDependentOnThisEntity()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = $this->cacheTags;
    foreach ($this->conditions as $condition) {
      $tags = array_merge($tags, $condition->getCacheTags());
    }
    return Cache::mergeTags($tags, []);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $cache_contexts = $this->cacheContexts;
    foreach ($this->conditions as $condition) {
      $cache_contexts = array_merge($cache_contexts, $condition->getCacheContexts());
    }
    return Cache::mergeContexts($cache_contexts);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    $max_age = $this->cacheMaxAge;
    foreach ($this->conditions as $condition) {
      $max_age = Cache::mergeMaxAges($max_age, $condition->getCacheMaxAge());
    }
    return $max_age;
  }

}
