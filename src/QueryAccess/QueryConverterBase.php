<?php

namespace Drupal\entity_unified_access\QueryAccess;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\entity_unified_access\ConditionConverter\ConditionConverterBase;
use Drupal\entity_unified_access\Conditions\ConditionGroup;
use Drupal\entity_unified_access\Conditions\ConditionInterface;

/**
 * Class QueryConverterBase
 *
 * @internal
 */
abstract class QueryConverterBase extends ConditionConverterBase {

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper|null $resultGroup
   *
   * @return \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper
   */
  public function convert(ConditionInterface $condition, $resultGroup = NULL) {
    /** @var \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper $result */
    $result = parent::convert($condition);
    return $result;
  }

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper $result
   *
   * @return \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper
   */
  protected function addCacheableDependency($result, CacheableDependencyInterface $cacheableDependency) {
    // We do this in the alter hook.
    return $result;
  }

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper $result
   *
   * @return \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper
   */
  protected function setDependentOnThisEntity($result) {
    // We can safely ignore isDependentOnThisEntity(), as the query carries
    // the list tag anyway.
    return $result;
  }

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper|null $outerResultGroup
   *
   * @return \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper
   */
  protected function getConstantCondition(ConditionInterface $condition, $outerResultGroup = NULL) {
    $value = $condition->getConstantValue();
    $wrapper = $this->getSqlConstantConditionWrapper($value);
    return $wrapper;
  }

  /**
   * @param bool $value
   *
   * @return \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper
   */
  protected function getSqlConstantConditionWrapper($value) {
    // As core does not support a single condition, we add an arbtrary contaner
    // with a single condition that will be optimized away anyway.
    $sqlCondition = $this->getSqlConstantCondition($value);
    $wrapper = new SqlConditionWrapper($sqlCondition);
    return $wrapper;
  }

  /**
   * @param $value
   *
   * @return \Drupal\Core\Database\Query\ConditionInterface
   */
  protected function getSqlConstantCondition($value) {
    $sqlConditionGroup = $this->getSqlConditionGroup('AND');
    $sqlConditionGroup->where($value ? '1=1' : '1=0');
    return $sqlConditionGroup;
  }

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper|null $outerResultGroup
   *
   * @return \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper
   */
  protected function getConditionGroup(ConditionGroup $group, $outerResultGroup = NULL) {
    // Conditions inside OR need the more expensive LEFT JOINs.
    // @see \Drupal\entity_unified_access\QueryAccess\EntityQueryConverter::getFieldCondition
    $alreadyNestedInsideOr = $outerResultGroup && $outerResultGroup->isNestedInsideOr();
    $nontrivialOrGroup = $group->getConjunction() === 'OR' && $group->count() > 1;
    $nestedInsideOr = $alreadyNestedInsideOr || $nontrivialOrGroup;
    $sqlCondition = $this->getSqlConditionGroup($group->getConjunction());
    $wrapper = new SqlConditionWrapper($sqlCondition, $nestedInsideOr);
    return $wrapper;
  }

  /**
   * Create a condition group.
   *
   * @param string $conjunction
   *
   * @return \Drupal\Core\Database\Query\ConditionInterface
   */
  abstract protected function getSqlConditionGroup($conjunction);

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper $resultGroup
   * @param \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper $resultCondition
   *
   * @return \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper
   */
  protected function addConditionToGroup($resultGroup, $resultCondition, ConditionGroup $group, ConditionInterface $condition) {
    $resultGroup->getSqlCondition()->condition($resultCondition->getSqlCondition());
    return $resultGroup;
  }

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper $resultGroup
   *
   * @return \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper
   */
  protected function finalizeGroup($resultGroup, ConditionGroup $group) {
    if (!count($resultGroup->getSqlCondition())) {
      // Provide safe default values for empty condition groups.
      $resultGroup->getSqlCondition()->condition($this->getSqlConstantCondition($group->getEmptyValue()));
    }
    return $resultGroup;
  }

  /**
   * Create field condition and fix edge case of empty array.
   *
   * @param $field
   *   The field.
   * @param $value
   *   The value.
   * @param $operator
   *   The operator.
   *
   * @return mixed
   */
  protected function getSqlFieldCondition($field, $value, $operator) {
    if (empty($value) && is_array($value)) {
      // The sql condition does not eat this.
      // @todo Move this code to FieldCondition::getConstantValue
      if ($operator === 'IN') {
        $sqlConditionWrapper = $this->getSqlConstantConditionWrapper(FALSE);
      }
      // $operator === 'NOT IN'
      else {
        $sqlConditionWrapper = $this->getSqlConstantConditionWrapper(TRUE);
      }
    }
    else {
      // As core does not support a single condition, we add an arbitrary
      // container with a single condition that will be optimized away anyway.
      $sqlCondition = $this->getSqlConditionGroup('AND');
      $sqlCondition->condition($field, $value, $operator);
      $sqlConditionWrapper = new SqlConditionWrapper($sqlCondition);
    }
    return $sqlConditionWrapper;
  }

}
