<?php

namespace Drupal\entity_unified_access\EntityAccess;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Entity\Sql\TableMappingInterface;
use Drupal\Core\TypedData\ComplexDataDefinitionInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\DataReferenceInterface;
use Drupal\Core\TypedData\ListInterface;

/**
 * Class EntitySingleQuery
 */
class EntitySingleQuery {

  /**
   * The fieldable entity.
   *
   * @var FieldableEntityInterface
   */
  protected $entity;

  /**
   * SingleEntityQuery constructor.
   *
   * @internal Use the static constructor.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The fieldable entity.
   */
  public function __construct(FieldableEntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Static constructor.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The fieldable entity.
   *
   * @return static
   */
  public static function create(FieldableEntityInterface $entity) {
    return new static($entity);
  }

  /**
   * Get property values.
   *
   * @param string $propertyPath
   *   The property path.
   *
   * @return \Drupal\entity_unified_access\EntityAccess\EntitySingleQueryResult
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function query($propertyPath) {
    $resultArray = $this->doQuery(EntityAdapter::createFromEntity($this->entity), $propertyPath, $this->entity->getEntityTypeId());
    $result = new EntitySingleQueryResult($resultArray);
    return $result;
  }

  /**
   * Map property values.
   *
   * This kinda imitates EntityFieldQuery. The main difference is, as we imitate
   * a sql query, if we get a property from an item list, we array_map it.
   *
   * @param \Drupal\Core\TypedData\TypedDataInterface $item
   *   The data item to query.
   * @param string $propertyPathr
   *   The property path to query.
   *
   * @param string $prefix
   *   The property path ancestors of $item.
   *
   * @return mixed[]
   *   The result array.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @todo Implement case insensitive field comparisons.
   *
   * @todo Implement %delta.
   * @see \Drupal\Core\Entity\Query\Sql\Tables::addField
   */
  protected function doQuery($item, $propertyPathr, $prefix) {
    $result = [];
    $parts = explode('.', $propertyPathr, 2);
    $property = $parts[0];
    $remaining = isset($parts[1]) ? $parts[1] : NULL;

    if ($property === TableMappingInterface::DELTA) {
      throw new \UnexpectedValueException('Sorry, %delta is not yet implemented.');
    }

    // Get all deltas: Someone requested field_foo.value. Expand this to
    // field_foo.$i.value for every $i.
    if ($item instanceof ListInterface && !is_numeric($property)) {
      foreach ($item as $i => $_) {
        $result += $this->doQuery($item, "$i.$propertyPathr", $prefix);
      }
    }
    else {
      // Get a single property value.
      if (method_exists($item, 'get')) {
        try {
          $resultItem = $item->get($property);
        }
        catch (\InvalidArgumentException $e) {
          $resultItem = NULL;
        }
      }
      else {
        $class = get_class($item);
        throw new \UnexpectedValueException("Can not get property $property of $class as it has no get() method.");
      }

      // Dereference references.
      if ($resultItem instanceof DataReferenceInterface) {
        $resultItem = $resultItem->getTarget();
      }

      if (isset($resultItem)) {
        if (isset($remaining)) {
          // Recurse.
          $result += $this->doQuery($resultItem, $remaining, "$prefix$property.");
        }
        else {
          // If we're finished but don't have a scalar yet, try to amend what we
          // can guess.
          if (
            $resultItem instanceof ComplexDataInterface
            && ($mainPropertyName = $resultItem->getDataDefinition()->getMainPropertyName())
          ) {
            // Someone requested field_foo.0 and meant field_foo.0.value.
            $result += $this->doQuery($resultItem, $mainPropertyName, "$prefix$property.");
          }
          elseif (
            $resultItem instanceof ListInterface
            && ($resultItemDefinition = $resultItem->getItemDefinition())
            && $resultItemDefinition instanceof ComplexDataDefinitionInterface
          ) {
            // Someone requested field_foo and meant field_foo.$i.value.
            // Request value then. The all-deltas logic above will do the rest.
            $mainPropertyName = $resultItemDefinition->getMainPropertyName();
            $result += $this->doQuery($resultItem, $mainPropertyName, "$prefix$property.");
          }
          else {
            // Add it to the result. ::checkConditionOnTypedDataArray will yell
            // if this is not a scalar.
            $result["$prefix.$property"] = $resultItem->getValue();
          }
        }
      }
    }

    return $result;
  }

}
