<?php

namespace Drupal\entity_unified_access\QueryAccess;

use Drupal\Core\Database\Query\ConditionInterface;

/**
 * Class SqlConditionWrapper
 */
class SqlConditionWrapper {

  /**
   * The SQL condition.
   *
   * @var \Drupal\Core\Database\Query\ConditionInterface
   */
  protected $sqlCondition;

  /**
   * Whether the access conditions are nested inside an OR condition.
   *
   * This means we can not use the faster inner join, but must use left join.
   *
   * @var bool
   */
  protected $nestedInsideOr;

  /**
   * SqlConditionWrapper constructor.
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $sqlCondition
   */
  public function __construct(ConditionInterface $sqlCondition, $nestedInsideOr = FALSE) {
    $this->sqlCondition = $sqlCondition;
    $this->nestedInsideOr = $nestedInsideOr;
  }

  /**
   * @return \Drupal\Core\Database\Query\ConditionInterface|\Countable
   */
  public function getSqlCondition() {
    return $this->sqlCondition;
  }

  /**
   * @return bool
   */
  public function isNestedInsideOr() {
    return $this->nestedInsideOr;
  }

}
