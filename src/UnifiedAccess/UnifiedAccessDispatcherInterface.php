<?php

namespace Drupal\entity_unified_access\UnifiedAccess;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;

interface UnifiedAccessDispatcherInterface {

  /**
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type.
   * @param string $operation
   *   The operation.
   * @param \Drupal\Core\Session\AccountInterface|NULL $account
   *   The user account.
   *
   * @return \Drupal\entity_unified_access\Conditions\AndConditionGroup
   */
  public function dispatch(EntityTypeInterface $entityType, $operation, AccountInterface $account = NULL);

}
