<?php

namespace Drupal\entity_unified_access\EntityAccess;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_unified_access\UnifiedAccess\UnifiedAccessDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityAccessAlter.
 *
 * @internal
 */
class EntityAccessAlter implements ContainerInjectionInterface {

  /**
   * The unified access dispatcher.
   *
   * @var \Drupal\entity_unified_access\UnifiedAccess\UnifiedAccessDispatcherInterface
   */
  protected $unifiedAccessDispatcher;

  /**
   * Constructs a new EntityAccessAlter object.
   *
   * @param \Drupal\entity_unified_access\UnifiedAccess\UnifiedAccessDispatcherInterface $unifiedAccessDispatcher
   *   The unified access dispatcher.
   */
  public function __construct(UnifiedAccessDispatcherInterface $unifiedAccessDispatcher) {
    $this->unifiedAccessDispatcher = $unifiedAccessDispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.unified_access.dispatcher')
    );
  }

  /**
   * Implements hook_entity_access().
   *
   * This returns AccessResult::neutral of ::forbidden if unified access is
   * implemented.
   *
   * @todo Consider making this configurable.
   * @todo Kill that insane 3state access object upstream.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param $operation
   *   The operation.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultInterface|null
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function hookEntityAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($entity instanceof FieldableEntityInterface) {
      $conditions = $this->unifiedAccessDispatcher->dispatch($entity->getEntityType(), $operation, $account);
      if (count($conditions)) {
        $allowedOrNeutral = (new EntitySingleQueryConverter($entity))->convert($conditions);
        $neutralOrForbidden = $allowedOrNeutral->isAllowed() ? AccessResult::neutral() : AccessResult::forbidden();
        $neutralOrForbidden->addCacheableDependency($allowedOrNeutral);
        return $neutralOrForbidden;
      }
    }
  }

}
