<?php

namespace Drupal\entity_unified_access\UnifiedAccess;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_unified_access\Conditions\AndConditionGroup;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UnifiedAccessDispatcher implements UnifiedAccessDispatcherInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new QueryAccessHandlerBase object.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher, AccountInterface $current_user) {
    $this->eventDispatcher = $event_dispatcher;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function dispatch(EntityTypeInterface $entityType, $operation, AccountInterface $account = NULL) {
    $account = $account ?: $this->currentUser;
    $entity_type_id = $entityType->id();
    $conditions = new AndConditionGroup('UnifiedAccessDispatcher');
    // Allow other modules to modify the conditions before they are used.
    $event = new UnifiedAccessEvent($entityType, $conditions, $operation, $account);
    $this->eventDispatcher->dispatch($event, "entity.unified_access.{$entity_type_id}");

    return $conditions;
  }

}
