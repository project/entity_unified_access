<?php

namespace Drupal\entity_unified_access\Conditions;

final class AndConditionGroup extends ConditionGroup {

  use CreateByNameTrait;

  /**
   * {@inheritDoc}
   */
  public function getConjunction() {
    return 'AND';
  }

  /**
   * @inheritDoc
   */
  public function getEmptyValue() {
    return TRUE;
  }

}
