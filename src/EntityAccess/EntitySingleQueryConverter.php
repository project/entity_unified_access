<?php

namespace Drupal\entity_unified_access\EntityAccess;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\entity_unified_access\ConditionConverter\ConditionConverterBase;
use Drupal\entity_unified_access\Conditions\ConditionGroup;
use Drupal\entity_unified_access\Conditions\ConditionInterface;
use Drupal\entity_unified_access\Conditions\ConstantCondition;
use Drupal\entity_unified_access\Conditions\FieldCondition;

/**
 * Class EntitySingleQueryConverter.
 */
class EntitySingleQueryConverter extends ConditionConverterBase {

  /**
   * @var \Drupal\Core\Entity\FieldableEntityInterface
   */
  protected $entity;

  /**
   * EntityAccessReplay constructor.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   */
  public function __construct(FieldableEntityInterface $entity, $simplify = TRUE) {
    $this->entity = $entity;
    parent::__construct($simplify);
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  public function convert(ConditionInterface $condition, $resultGroup = NULL) {
    return parent::convert($condition);
  }

  /**
   * Create a constant condition.
   *
   * @param bool $value
   *   The boolean value.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   */
  protected function createAccessResult($value) {
    return $value ?
      $accessToAdd = AccessResult::allowed() :
      AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  protected function getConstantCondition(ConditionInterface $condition, $outerResultGroup = NULL) {
    return $this->createAccessResult($condition->getConstantValue());
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  protected function getConditionGroup(ConditionGroup $group, $outerResultGroup = NULL) {
    return $this->createAccessResult($group->getEmptyValue());
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Access\AccessResultInterface $resultGroup
   *   The group.
   * @param \Drupal\Core\Access\AccessResultInterface $resultCondition
   *   The condition to add.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  protected function addConditionToGroup($resultGroup, $resultCondition, ConditionGroup $group, ConditionInterface $condition) {
    $conjunction = $group->getConjunction();
    if ($conjunction === 'OR') {
      return $resultGroup->orIf($resultCondition);
    }
    elseif ($conjunction === 'AND') {
      return $resultGroup->andIf($resultCondition);
    }
    else {
      throw new \UnexpectedValueException("Unexpected conjunction: $conjunction");
    }
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Access\AccessResultInterface $resultGroup
   *   The group.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  protected function finalizeGroup($resultGroup, ConditionGroup $group) {
    return $resultGroup;
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getFieldCondition(FieldCondition $condition, $outerResultGroup = NULL) {
    $query = EntitySingleQuery::create($this->entity);
    $value = $query->query($condition->getField())
      ->checkCondition($condition->getValue(), $condition->getOperator());
    $access = $this->createAccessResult($value);
    $access->addCacheableDependency($condition);
    if ($condition->isDependentOnThisEntity()) {
      $access->addCacheableDependency($this->entity);
    }
    return $access;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Access\AccessResultInterface|RefinableCacheableDependencyInterface $result
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  protected function addCacheableDependency($result, CacheableDependencyInterface $cacheableDependency) {
    return $result->addCacheableDependency($cacheableDependency);
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Access\AccessResultInterface|RefinableCacheableDependencyInterface $result
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  protected function setDependentOnThisEntity($result) {
    return $result->addCacheableDependency($this->entity);
  }

}
