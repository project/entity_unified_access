<?php

namespace Drupal\entity_unified_access;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Cacheability utility
 *
 * @internal
 */
class CacheabilityUtility {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * CacheabilityUtility constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   */
  public function __construct(RendererInterface $renderer, RequestStack $requestStack) {
    $this->renderer = $renderer;
    $this->requestStack = $requestStack;
  }

  /**
   * Applies the cacheablity metadata to the current request.
   *
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheable_metadata
   *   The cacheability metadata.
   *
   * @throws \Exception
   */
  public function applyCacheabilityToRequest(CacheableDependencyInterface $cacheable_metadata) {
    $request = $this->requestStack->getCurrentRequest();
    if ($request->isMethodCacheable() && $this->renderer->hasRenderContext()) {
      $build = [];
      $cacheable_metadata->applyTo($build);
      $this->renderer->render($build);
    }
  }

}
