<?php

namespace Drupal\entity_unified_access\QueryAccess;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\entity_unified_access\CacheabilityUtility;
use Drupal\entity_unified_access\UnifiedAccess\UnifiedAccessDispatcherInterface;
use Drupal\views\ViewExecutable;

/**
 * Defines a class for altering SQL queries.
 *
 * @internal
 */
class SqlQueryAlterEntityType {

  /**
   * The unified access dispatcher.
   *
   * @var \Drupal\entity_unified_access\UnifiedAccess\UnifiedAccessDispatcherInterface
   */
  protected $unifiedAccessDispatcher;

  /**
   * @var \Drupal\entity_unified_access\CacheabilityUtility
   */
  protected $cacheabilityUtility;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * @var \Drupal\Core\Database\Query\SelectInterface
   */
  protected $query;

  /**
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * @var \Drupal\Core\Entity\Sql\DefaultTableMapping
   */
  protected $tableMapping;

  /**
   * @var $string
   */
  protected $baseTableName;

  /**
   * @var bool
   */
  protected $usesRevisions;

  /**
   * SqlQueryAlterEntityType constructor.
   *
   * @param \Drupal\entity_unified_access\UnifiedAccess\UnifiedAccessDispatcherInterface $unifiedAccessDispatcher
   * @param \Drupal\entity_unified_access\CacheabilityUtility $cacheabilityUtility
   * @param \Drupal\Core\Database\Connection $connection
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   * @param \Drupal\Core\Entity\Sql\DefaultTableMapping $tableMapping
   */
  public function __construct(UnifiedAccessDispatcherInterface $unifiedAccessDispatcher, CacheabilityUtility $cacheabilityUtility, Connection $connection, SelectInterface $query, EntityTypeInterface $entityType, DefaultTableMapping $tableMapping) {
    $this->unifiedAccessDispatcher = $unifiedAccessDispatcher;
    $this->cacheabilityUtility = $cacheabilityUtility;
    $this->connection = $connection;
    $this->query = $query;
    $this->entityType = $entityType;
    $this->tableMapping = $tableMapping;
  }

  protected function init() {
    $this->initBaseTableInfo();
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function alter() {
    if (!$account = $this->query->getMetaData('account')) {
      $account = \Drupal::currentUser();
    }
    if (!$operation = $this->query->getMetaData('op')) {
      $operation = 'view';
    }

    // Create subquery so we introduce no duplicates.
    $subQuery = $this->createSubQuery();
    $converter = SqlQueryConverter::create($subQuery, $this->entityType);
    $conditions = $this->unifiedAccessDispatcher->dispatch($this->entityType, $operation, $account);
    if (count($conditions)) {
      $wrapper = $converter->convert($conditions);
      $sqlCondition = $wrapper->getSqlCondition();
      $subQuery->condition($sqlCondition);
      // We might have to join this in more than once.
      foreach ($this->tableMapping as $tableAlias => $tableInfo) {
        if ($tableInfo['table'] === $this->baseTableName) {
          $currentSubQuery = clone $subQuery;
          $id = $this->usesRevisions ?
            $this->entityType->getKey('revision') :
            $this->entityType->getKey('id');
          $currentSubQuery->where("$tableAlias.$id = eua.$id");
          $this->query->exists($currentSubQuery);
        }
      }

      // Extract cacheability directly from the conditions. We can safely ignore
      // isDependentOnThisEntity(), as the query carries the list tag anyway.
      $cacheability = CacheableMetadata::createFromObject($conditions);

      // Apply a dirty hack that adds our cacheability to the request by
      // rendering it.
      $this->cacheabilityUtility->applyCacheabilityToRequest($cacheability);

      // If this is a views query, pass cacheability via this dirty hack, so the
      // views cache can pick it up.
      // @todo Make views pick up metadata.
      $view = $this->query->getMetaData('view');
      if ($view instanceof ViewExecutable) {
        $view->storage->addCacheTags($cacheability->getCacheTags());
        $view->storage->addCacheContexts($cacheability->getCacheContexts());
      }

      // Pass cacheability back via metadata.
      $queryCacheability = $this->query->getMetaData('cacheabilty') ?: new CacheableMetadata();
      $queryCacheability->addCacheableDependency($cacheability);
      $this->query->addMetaData('cacheability', $queryCacheability);
    }
  }

  /**
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function createSubQuery() {
    $subQueryBaseTable = $this->usesRevisions ?
      $this->tableMapping->getRevisionDataTable() :
      $this->tableMapping->getDataTable();

    // Subquery table alias seems to override main query so no hassle here.
    $subQuery = $this->connection->select($subQueryBaseTable, 'eua');
    $subQuery->fields('eua', ['1']);

    $subQuery->addMetaData('language', $this->query->getMetaData('language'));
    // @see \Drupal\Core\Entity\Query\Sql\Tables::addField
    $subQuery->addMetaData('entity_type', $this->entityType->id());
    $subQuery->addMetaData('all_revisions', $this->usesRevisions);

    return $subQuery;
  }

  protected function initBaseTableInfo() {
    $baseTableFromMetadata = $this->query->getMetaData('base_table');
    if ($baseTableFromMetadata) {
      $found = $this->findTable($baseTableFromMetadata);
    }
    else {
      // Check revision tables first.
      $preferredTables = [
        $this->tableMapping->getRevisionDataTable(),
        $this->tableMapping->getRevisionTable(),
        $this->tableMapping->getDataTable(),
        $this->tableMapping->getBaseTable(),
      ];

      foreach ($preferredTables as $preferredTable) {
        if ($found = $this->findTable($preferredTable)) {
          break;
        }
      }
    }

    if (!$found) {
      throw new \LogicException("Query tagged for {$this->entityType->id()} access, but there is no base table, specify the base_table using base_table metadata.");
    }

    $revisionTables = [
      $this->tableMapping->getRevisionDataTable(),
      $this->tableMapping->getRevisionTable(),
    ];
    $this->usesRevisions = in_array($this->baseTableName, $revisionTables);
  }

  /**
   * @param string $preferredTables
   *
   * @return bool
   */
  protected function findTable($preferredTable) {
    foreach ($this->query->getTables() as $tableInfo) {
      if (!($tableInfo instanceof SelectInterface)) {
        if ($tableInfo['table'] === $preferredTable) {
          $this->baseTableName = $tableInfo['table'];
          return TRUE;
        }
      }
    }
    return FALSE;
  }

}
