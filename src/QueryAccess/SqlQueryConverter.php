<?php

namespace Drupal\entity_unified_access\QueryAccess;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\Sql\Tables;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\entity_unified_access\Conditions\FieldCondition;

/**
 * Class SqlQueryConverter.
 */
class SqlQueryConverter extends QueryConverterBase {

  /**
   * @var \Drupal\Core\Database\Query\SelectInterface
   */
  protected $query;

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * EntityQueryConverter constructor.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   * @param $simplify
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @internal Use ::create.
   */
  public function __construct(SelectInterface $query, EntityTypeInterface $entityType, $simplify) {
    $this->query = $query;
    $this->entityType = $entityType;
    parent::__construct($simplify);
  }

  /**
   * Create a entity query converter.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   * @param bool $simplify
   *   Whether the query should be simplified.
   *
   * @return \Drupal\entity_unified_access\QueryAccess\SqlQueryConverter
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function create(SelectInterface $query, EntityTypeInterface $entityType, $simplify = TRUE) {
    return new static($query, $entityType, $simplify);
  }

  /**
   * {@inheritdoc}
   */
  protected function getSqlConditionGroup($conjunction) {
    return $this->query->conditionGroupFactory($conjunction);
  }

  /**

   * {@inheritdoc}
   *
   * @param \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper $outerResultGroup
   *
   * @return \Drupal\entity_unified_access\QueryAccess\SqlConditionWrapper
   *
   * @throws \Drupal\Core\Entity\Query\QueryException
   */
  protected function getFieldCondition(FieldCondition $condition, $outerResultGroup = NULL) {
    $langcode = $this->query->getMetaData('language');
    $nested_inside_or = $outerResultGroup ? $outerResultGroup->isNestedInsideOr() : FALSE;
    $type = $nested_inside_or || $condition->getOperator() === 'IS NULL' ? 'LEFT' : 'INNER';
    $tables = new Tables($this->query);
    $sql_field = $tables->addField($condition->getField(), $type, $langcode);
    $value = $condition->getValue();
    $operator = $condition->getOperator();
    // Using LIKE/NOT LIKE ensures a case insensitive comparison.
    // @see \Drupal\Core\Entity\Query\Sql\Condition::translateCondition().
    $case_sensitive = $tables->isFieldCaseSensitive($condition->getField());
    $operator_map = [
      '=' => 'LIKE',
      '<>' => 'NOT LIKE',
    ];
    if (!$case_sensitive && isset($operator_map[$operator])) {
      $operator = $operator_map[$operator];
      $value = $this->query->escapeLike($value);
    }

    return $this->getSqlFieldCondition($sql_field, $value, $operator);
  }

}
