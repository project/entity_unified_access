<?php


namespace Drupal\entity_unified_access\Conditions;

trait CreateByNameTrait {

  /**
   * {@inheritdoc}
   */
  public static function create($name) {
    return new static($name);
  }

}
