<?php

namespace Drupal\entity_unified_access\EntityAccess;

/**
 * Class EntitySingleQueryResult.
 */
class EntitySingleQueryResult {

  /**
   * The values.
   *
   * @var mixed[]
   */
  protected $values;

  /**
   * SingleEntityQueryResult constructor.
   *
   * @param mixed[] $values
   *   The values.
   */
  public function __construct(array $values) {
    $this->values = $values;
  }

  /**
   * Check if there is a value in the list that satisfies the condition.
   *
   * @param $value
   *   The value to check.
   * @param $operator
   *   The operator to check.
   *
   * @return bool
   *   The result.
   */
  public function checkCondition($value, $operator) {
    foreach ($this->values as $propertyPath => $valueToCheck) {
      if (!is_scalar($valueToCheck)) {
        $valueDump = var_export($valueToCheck, 1);
        throw new \UnexpectedValueException("Can not check nonscalar $propertyPath $operator $value: $valueDump.");
      }
      if ($this->checkConditionOnValue($valueToCheck, $value, $operator)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Check a literal condition.
   *
   * @param $value1
   *   The first value.
   * @param $value2
   *   The second value.
   * @param string $operator
   *   The operator.
   *
   * @return bool
   *   The result.
   *
   * @see \Drupal\entity_unified_access\Conditions\FieldCondition::$supportedOperators
   */
  protected function checkConditionOnValue($value1, $value2, $operator) {
    switch ($operator) {
      case '=': return $value1 === $value2;

      case '<>': return $value1 !== $value2;

      case '<': return $value1 < $value2;

      case '<=': return $value1 <= $value2;

      case '>': return $value1 > $value2;

      case '>=': return $value1 >= $value2;

      case 'BETWEEN': return $value1 >= $value2[0] && $value1 <= $value2[1];

      case 'NOT BETWEEN': return !($value1 >= $value2[0] && $value1 <= $value2[1]);

      case 'IN': return in_array($value1, $value2);

      case 'NOT IN': return !(in_array($value1, $value2));

      case 'IS NULL': return is_null($value1);

      case 'IS NOT NULL': return !is_null($value1);

      default: throw new \UnexpectedValueException("Unexpected operator: $operator");
    }
  }

}
