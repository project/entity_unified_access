<?php

namespace Drupal\entity_unified_access\UnifiedAccess;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_unified_access\Conditions\ConditionGroup;

/**
 * Defines the common base class for query and entity access event.
 */
class UnifiedAccessEvent extends Event {

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * The conditions.
   *
   * @var \Drupal\entity_unified_access\Conditions\ConditionGroup
   */
  protected $conditions;

  /**
   * The operation.
   *
   * @var string
   */
  protected $operation;

  /**
   * The user for which to restrict access.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Constructs a new QueryAccessEvent.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type.
   * @param \Drupal\entity_unified_access\Conditions\ConditionGroup $conditions
   *   The conditions.
   * @param string $operation
   *   The operation. Usually one of "view", "update", "duplicate", or "delete".
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user for which to restrict access.
   */
  public function __construct(EntityTypeInterface $entityType, ConditionGroup $conditions, $operation, AccountInterface $account) {
    $this->entityType = $entityType;
    $this->conditions = $conditions;
    $this->operation = $operation;
    $this->account = $account;
  }

  /**
   * Gets the entity type.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   */
  public function getEntityType() {
    return $this->entityType;
  }

  /**
   * Gets the conditions.
   *
   * If $conditions->isAlwaysFalse() is TRUE, the user doesn't have access to
   * any entities, and the query is expected to return no results.
   * This can be reversed by calling $conditions->alwaysFalse(FALSE).
   *
   * If $conditions->isAlwaysFalse() is FALSE, and the condition group is
   * empty (count is 0), the user has full access, and the query doesn't
   * need to be restricted.
   *
   * @return \Drupal\entity_unified_access\Conditions\ConditionGroup
   *   The conditions.
   */
  public function getConditions() {
    return $this->conditions;
  }

  /**
   * Gets the operation.
   *
   * @return string
   *   The operation. Usually one of "view", "update" or "delete".
   */
  public function getOperation() {
    return $this->operation;
  }

  /**
   * Gets the user for which to restrict access.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   The user.
   */
  public function getAccount() {
    return $this->account;
  }

}
